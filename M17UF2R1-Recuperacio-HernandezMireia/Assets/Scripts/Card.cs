using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Carta")]
public class Card : ScriptableObject
{
    public Sprite cardTemplate;
    public new string name;
    public string description;
    public Sprite image;
    public Sprite pixelImage;
    public bool[,] victoria;
   
}

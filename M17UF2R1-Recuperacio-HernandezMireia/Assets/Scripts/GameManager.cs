using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public AudioSource clickSound;
    public AudioSource winSound;
    public AudioSource lostSound;


    public Card[] cards;
    public SpriteRenderer[] boxes;
    public Sprite Box;

    private Card cardSelec;
    private Card rivalCard;

    private int playerPoints;
    private int rivalPoints;
    // Start is called before the first frame update
    void Start()
    {
        rivalCard = cards[Random.Range(0, cards.Length)];
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            clickSound.Play();
        }
    }

    public void CardSelection( int index)
    {
        cardSelec = cards[index];
        bool playerWins = cardSelec.victoria[index, GetRivalIndex()];
        bool rivalWins = rivalCard.victoria[ GetRivalIndex(), index];

        if (playerWins)
        {
            playerPoints++;
            DestroyRivalBox();
            winSound.Play();
        }else if(rivalWins){
            rivalPoints++;
            DestroyPlayerBox();
            lostSound.Play();

        }


        if(playerPoints >=6 ||rivalPoints >= 6)
        {
            if(playerPoints > rivalPoints)
            {
                Debug.Log("Player Wins");
            }else if(playerPoints < rivalPoints)
            {
                Debug.Log("Rival Wins");
            }
            else
            {
                Debug.Log("No one Wins");
            }

            playerPoints = 0;
            rivalPoints = 0;
            rivalCard = cards[Random.Range(0, cards.Length)];

            for(int i =0; i < boxes.Length; i++)
            {
                boxes[i].sprite = Box;
            }
        }
    }

    private int GetRivalIndex()
    {
        for(int i=0; i< cards.Length; i++)
        {
            if(cards[i] == rivalCard)
            {
                return i;
            }
        }
        return -1;
    }


    private void DestroyRivalBox()
    {
        for(int i =0; i< boxes.Length; i++)
        {
           if( boxes[i].sprite == Box)
            {
                boxes[i].sprite = Box;
                break;
            }
        }
    }

    private void DestroyPlayerBox() {
        for (int i = 0; i < boxes.Length; i++)
        {
            if (boxes[i].sprite == Box)
            {
                boxes[i].sprite = Box;
                break;
            }
        }
    }


  
}
